# CoronaBot

O CoronaBot é um chatbot simples desenvolvido usando [Angular](https://angular.io), [Node](https://nodejs.org/) e [Socket.io](https://socket.io) 

Ele fornece informações atualizadas do número de casos confirmados e do total de mortes pela COVID-19.

As informações são provenientes de uma [API](https://documenter.getpostman.com/view/10808728/SzS8rjbc?version=latest#intro) externa.

## Instalação

É necessário ter o NodeJS instalado.

Na raiz do projeto digite o seguinte comando:

```
cd client && npm install && cd ../server && npm install
```

## Utilização

Na pasta client, execute:
```
npm start
```

Na pasta server, execute:
```
node index.js
```

Abra o browser e acesse: http://localhost:4200

Pronto, agora é só  usar!