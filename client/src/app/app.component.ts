import { AfterViewChecked, Component, ElementRef, OnInit, ViewChild } from '@angular/core';

import { ChatService } from './services/chat-service';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit, AfterViewChecked {
  @ViewChild("messageBox") private messageBox: ElementRef;

  title = "chatbot";
  message: string;
  messages: string[] = [];
  index: number;

  constructor(private chatService: ChatService) {}

  ngOnInit() {
    this.chatService.getMessages().subscribe((message: string) => {
      this.scrollToBottom();
      this.messages.push(message);
    });
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  sendMessage() {
    this.chatService.sendMessage(this.message);
    this.message = "";
  }

  scrollToBottom(): void {
    try {
      this.messageBox.nativeElement.scrollTop = this.messageBox.nativeElement.scrollHeight;
    } catch (err) {}
  }
}
