const express = require('express');
const app = express();
const axios = require('axios').default;
const http = require('http');
const server = http.Server(app);

const socketIO = require('socket.io');
const io = socketIO(server);

const port = process.env.PORT || 3000;

io.on('connection', (socket) => {
    let answers = 0;

    socket.on('new-message', async (message) => {
        io.emit('new-message', message);

        const reply = (reply) => {
            setTimeout(() => {
                io.emit('new-message', reply);
            }, 1000);
        };

        if (answers === 0) {
            reply('Olá, tudo bem? Gostaria de receber panorama do Corona Vírus no Brasil? S ou N?');
            answers++;
        } else if (answers > 0 && message.toUpperCase() !== 'S' && message.toUpperCase() !== 'N') {
            reply('Não entendi :/ Responda S ou N, por favor');
        } else if (answers > 0 && message.toUpperCase() === 'S') {
            try {
                // API Data from: https://documenter.getpostman.com/view/10808728/SzS8rjbc?version=latest#intro
                const response = await axios.get('https://api.covid19api.com/summary');
                const brazilData = response.data.Countries.filter((e) => e.Country === 'Brazil')[0];
                reply(
                    `Total de casos confirmados: ${brazilData.TotalConfirmed}. Total de mortes: ${brazilData.TotalDeaths}`
                );
            } catch (error) {
                reply('Não foi possível coletar os dados, tente mais tarde.');
            }

            answers = 0;
        } else if (answers > 0 && message.toUpperCase() === 'N') {
            reply('Sem problemas, até a próxima!');
            answers = 0;
        }
    });
});

server.listen(port, () => {
    console.log(`started on port: ${port}`);
});
